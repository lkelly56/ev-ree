#include <SoftwareSerial.h>
#include <SerialCommand.h>
#include <Adafruit_NeoPixel.h>
SerialCommand sCmd;

int analogPin = A0;
int analogPin2 = A2;
int val = 0;
int val2 = 0;

//create a NeoPixel strip
Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(30, 6, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(30, 5, NEO_GRB + NEO_KHZ800);

void setup() {
  // start the strip and blank it out
  strip1.begin();
  strip1.show();

  strip2.begin();
  strip2.show();
  
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);

  sCmd.addCommand("ShowLed", showLed);
  sCmd.addCommand("HideLed", hideLed);

  sCmd.addCommand("ShowLed2", showLed2);
  sCmd.addCommand("HideLed2", hideLed2);
}

void loop() {
  
  val = analogRead(analogPin);
  val2 = analogRead(analogPin2);

  //Serial.println(val);
  Serial.println((String)val + "," + val2);
  delay(40);

  if (Serial.available() > 0)
  sCmd.readSerial();
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip1.numPixels(); i++) {
    strip1.setPixelColor(i, c);
    strip1.show();
    delay(wait);
  }
}

void colorWipe2(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip2.numPixels(); i++) {
    strip2.setPixelColor(i, c);
    strip2.show();
    delay(wait);
  }
}

void showLed (const char *command) {
  colorWipe(strip1.Color(0,255,217), 10);
}

void hideLed (const char *command) {
  colorWipe(strip1.Color(0,0,0), 10);
}

void showLed2 (const char *command) {
  colorWipe2(strip2.Color(0,255,217), 10);
}

void hideLed2 (const char *command) {
  colorWipe2(strip2.Color(0,0,0), 10);
}
