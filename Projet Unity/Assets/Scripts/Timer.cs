﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    // Image du timer
    public Image timerBar;

    // Temps restant
    public float timeLeft;

    // Temps maximal
    float maxTime = 120f;

    // Au lancement, on affecte le temps max au temps restant
    private void Start()
    {
        timeLeft = maxTime;
    }

    private void Update()
    {
        // Si le timer n'est pas fini
        if (timeLeft > 0)
        {
            // On réduit le temps et on met à jour l'image
            timeLeft -= Time.deltaTime;
            timerBar.fillAmount = timeLeft / maxTime;
        }
        // Sinon
        else
        {
            // On affiche la scène "Win"
            SceneManager.LoadScene(2);
        }
    }
}
