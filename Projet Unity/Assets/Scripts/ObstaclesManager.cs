﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Script relié à ObstaclesManager
// Génère des obstacles sur la plateforme
public class ObstaclesManager : MonoBehaviour
{
    // Tableau contenant les prefabs des obstacles
    public GameObject[] obstaclesPrefabs;

    // Liste faisant référence au TileManager (affecté dans Unity)
    public TileManager tileManager;

    // Référence au joueur (affecté dans unity)
    public GameObject player;

    // Nombre d'obstacles visibles à la fois
    private int amountObstaclesOnScreen = 3;

    // Distance minimale entre deux éléments
    private float distanceMinBetween = 25.0f;

    // Liste des obstacles présentements sur la plateforme
    private List<GameObject> activeObstacles;

    // Variable déterminant si le dernier obstacle créé est le tas de roches
    private bool lastObstacleIsRocks = false;

    // Variable du timer
    public Timer time;


    private void Start()
    {
        // Création de la liste
        activeObstacles = new List<GameObject>();

        // Affichage d'un obstacle
        SpawnObstacle();

        // Lancement de la coroutine pour les obstacles
        StartCoroutine(AddNewObstacle());
    }


    private void Update()
    {
        // Si on a au moins un obstacle de présent et qu'il est rendu derrière le joueur
        if (activeObstacles.Count > 0 && activeObstacles[0].transform.position.z < player.transform.position.z - (activeObstacles[0].GetComponent<BoxCollider>().bounds.size.z + 15))
        {
            // On supprime l'obstacle
            DeleteObstacle();
        }
    }

    // Ajoute un obstacle à un temps random
    private IEnumerator AddNewObstacle()
    {
        // S'il reste plus de 8 secondes au jeu
        while (time.timeLeft > 8)
        {
            // Si on a moins de 4 et plus que 0 obstacles présents ET il y a plus que 0 tuiles
            if (activeObstacles.Count < amountObstaclesOnScreen && (activeObstacles.Count > 0 || activeObstacles.Count <= 0) && tileManager.activeTiles.Count > 0)
            {
                float randDistance = 0;

                if (activeObstacles.Count > 0)
                {
                    // Génère une distance aléatoire entre 0 et  la différence entre la dernière tuile et le dernier obstacle ajoutés
                    randDistance = Random.Range(0, (tileManager.activeTiles[tileManager.activeTiles.Count - 1].transform.position.z - activeObstacles[activeObstacles.Count - 1].transform.position.z));

                    // Valeur absolue si jamais randDistance est négatif
                    randDistance = Mathf.Abs(randDistance);

                    // Ajout de la distance minimale
                    randDistance += distanceMinBetween;

                    // Si la position du dernier obstacle + la distance random est plus petit que la position de la dernière tuile apparue
                    if ((activeObstacles[activeObstacles.Count - 1].transform.position.z + randDistance) < tileManager.activeTiles[tileManager.activeTiles.Count - 1].transform.position.z)
                    {
                        // Fait en sorte d'éviter que les obstacles apparaissent là ou il y a des trous
                        for (int i = 0; i < tileManager.activeTiles.Count; i++)
                        {
                            if (tileManager.activeTiles[i].tag == "missingTile")
                            {
                                float debutTuile = (tileManager.activeTiles[i].transform.position.z - tileManager.activeTiles[i].GetComponent<BoxCollider>().bounds.size.z / 2) - 5.0f;
                                float finTuile = (tileManager.activeTiles[i].transform.position.z + tileManager.activeTiles[i].GetComponent<BoxCollider>().bounds.size.z / 2) + 5.0f;
                                float posObstacle = activeObstacles[activeObstacles.Count - 1].transform.position.z + randDistance;

                                if ((posObstacle > debutTuile)
                                    &&
                                    (posObstacle < finTuile))
                                {
                                    break;
                                }
                            }

                            else if (i == tileManager.activeTiles.Count - 1)
                            {
                                // Alors on peut ajouter un obstacle
                                SpawnObstacle(randDistance);
                            }
                        }
                    }
                }
                else
                {
                    randDistance = Random.Range(0, (tileManager.activeTiles[tileManager.activeTiles.Count - 1].transform.position.z - player.transform.position.z));

                    randDistance = Mathf.Abs(randDistance);

                    // Ajout de la distance minimale
                    randDistance += distanceMinBetween;

                    // Si la position du dernier obstacle + la distance random est plus petit que la position de la dernière tuile apparue
                    if ((player.transform.position.z + randDistance) < tileManager.activeTiles[tileManager.activeTiles.Count - 1].transform.position.z)
                    {
                        // Fait en sorte d'éviter que les obstacles apparaissent là ou il y a des trous
                        for (int i = 0; i < tileManager.activeTiles.Count; i++)
                        {
                            if (tileManager.activeTiles[i].tag == "missingTile")
                            {
                                float debutTuile = (tileManager.activeTiles[i].transform.position.z - tileManager.activeTiles[i].GetComponent<BoxCollider>().bounds.size.z / 2) - 5.0f;
                                float finTuile = (tileManager.activeTiles[i].transform.position.z + tileManager.activeTiles[i].GetComponent<BoxCollider>().bounds.size.z / 2) + 5.0f;
                                float posObstacle = player.transform.position.z + randDistance;

                                if ((posObstacle > debutTuile)
                                    &&
                                    (posObstacle < finTuile))
                                {
                                    break;
                                }
                            }

                            else if (i == tileManager.activeTiles.Count - 1)
                            {
                                // Alors on peut ajouter un obstacle
                                SpawnObstacle(randDistance);
                            }
                        }
                    }
                }

            }

            // On génère un temps random
            float randTime = Random.Range(2f, 5f);

            // Temps d'attente random
            yield return new WaitForSeconds(randTime);
        }

        // S'il reste 8 secondes au jeu, on enlève tous les obstacles
        DeleteAllObstacles();
    }


    // Fait apparaître un obstacle dans le jeu
    private void SpawnObstacle (float randDistance = 30.0f)
    {
        // Variable temporairement d'un obstacle
        GameObject obstacle;

        // Variable déterminant quel obstacle on fait apparaître
        GameObject whichObstacle = obstaclesPrefabs[0];

        // Random pour la probabilité d'apparition des roches
        float num = Random.Range(0, 100.0f);

        // Si num est entre 0 et 15, on affiche les roches
        if (num >= 0 && num <= 20 && !lastObstacleIsRocks)
        {
            whichObstacle = obstaclesPrefabs[obstaclesPrefabs.Length - 1];
            lastObstacleIsRocks = true;
        }

        // Sinon, on affiche les racines
        else
        {
            int randObstacle = Random.Range(0, obstaclesPrefabs.Length - 2);

            whichObstacle = obstaclesPrefabs[randObstacle];

            lastObstacleIsRocks = false;
        }

        // On instancie l'obstacle et on lui met le manager comme parent
        obstacle = Instantiate(whichObstacle) as GameObject;
        obstacle.transform.SetParent(transform);

        // Si on a au moins un obstacle présent, on l'ajoute après le dernier obstacle ajouté
        if (activeObstacles.Count > 0)
        {
            if (obstacle.tag == "obstacleSun")
            {
                obstacle.transform.position = new Vector3(player.transform.position.x+1, -0.50f, activeObstacles[activeObstacles.Count - 1].transform.position.z + randDistance);
            }

            else
            {
                obstacle.transform.position = new Vector3(player.transform.position.x, 0.25f, activeObstacles[activeObstacles.Count - 1].transform.position.z + randDistance);
            }
        }

        // On en ajoute un après la position du joueur
        else
        {
            if (obstacle.tag == "obstacleSun")
            {
                obstacle.transform.position = new Vector3(player.transform.position.x+1, -0.50f, player.transform.position.z + randDistance);
            }

            else
            {
                obstacle.transform.position = new Vector3(player.transform.position.x, 0.25f, player.transform.position.z + randDistance);
            }
        }

        // Ajout de l'obstacle au jeu
        activeObstacles.Add(obstacle);
    }

    // Détruit tous les obstacles présents
    private void DeleteAllObstacles()
    {
        for (int i = 0; i < activeObstacles.Count; i++)
        {
            Destroy(activeObstacles[i]);
            activeObstacles.RemoveAt(i);
        }
    }

    // Détruit le premier obstacle présent dans la liste (le plus vieux)
    private void DeleteObstacle ()
    {
        Destroy(activeObstacles[0]);
        activeObstacles.RemoveAt(0);
    }
}
