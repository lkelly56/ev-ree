﻿using System;
using System.IO.Ports;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalActivation : MonoBehaviour
{
    // Variables contenant les joueurs (affectés dans Unity)
    public GameObject player1;
    public GameObject player2;

    // Variables de scripts des joueurs
    private Player p1Script;
    private Player p2Script;

    // Positions de départ en x des joueurs
    private float posXPlayer1;
    private float posXPlayer2;

    // Variable des caméras
    public Camera camera1;
    public Camera camera2;

    // Variable de scripts des fogs
    private Fog fogScript1;
    private Fog fogScript2;

    // Variable des managers
    public GameObject tileManager1;
    public GameObject tileManager2;
    public GameObject obManager1;
    public GameObject obManager2;

    // Variables des scripts des managers
    private TileManager tileMScript1;
    private TileManager tileMScript2;
    private ObstaclesManager obMScript1;
    private ObstaclesManager obMScript2;

    private bool led1IsOn = false;
    private bool led2IsOn = false;

    // Variable déterminant si les joueurs ont échangé leur place ou non
    private bool areSwitched = false;

    // Variables pour l'arduino
    private SerialPort stream = new SerialPort("COM3", 9600);
    private string[] sensorsValues;

    private void Start()
    {
        // Afectation des positions en X
        posXPlayer1 = player1.transform.position.x;
        posXPlayer2 = player2.transform.position.x;

        // Affectation des scripts des joueurs
        p1Script = player1.GetComponent<Player>();
        p2Script = player2.GetComponent<Player>();

        // Affectation des fogs
        fogScript1 = camera1.GetComponent<Fog>();
        fogScript2 = camera2.GetComponent<Fog>();

        // Affection des manager
        tileMScript1 = tileManager1.GetComponent<TileManager>();
        tileMScript2 = tileManager2.GetComponent<TileManager>();

        obMScript1 = obManager1.GetComponent<ObstaclesManager>();
        obMScript2 = obManager2.GetComponent<ObstaclesManager>();


        // Arduino
        stream.ReadTimeout = 50;
        stream.Open();
    }

    private void Update()
    {
        // Lis les données du arduino
        string result = ReadFromArduino();

        if (result != null)
        {
            // Variables temporaires pour tous les éléments à inverser
            float posZPlayer1 = player1.transform.position.z;
            float posZPlayer2 = player2.transform.position.z;

            GameObject tempPlayerTM = tileMScript1.player;
            GameObject tempPlayerOM = obMScript1.player;
            TileManager tempTileManagerOM = obMScript1.tileManager;
            Color tempCameraColor = camera1.backgroundColor;
            Color tempFogColor = fogScript1.fogColor;

            // Sépération des données de l'arduino
            sensorsValues = result.Split(',');

            Debug.Log(sensorsValues[0] + ", " + sensorsValues[1]);

            if ((Int32.Parse(sensorsValues[0]) < 50 || Int32.Parse(sensorsValues[0]) > 1000) && !led1IsOn) {
                WriteToArduino("ShowLed");
                led1IsOn = true;
            }
            else if ( (Int32.Parse(sensorsValues[0]) > 500 && Int32.Parse(sensorsValues[0]) < 600) && led1IsOn) {
                WriteToArduino("HideLed");
                led1IsOn = false;
            }

            if ((Int32.Parse(sensorsValues[1]) < 50 || Int32.Parse(sensorsValues[1]) > 1000) && !led2IsOn) {
                WriteToArduino("ShowLed2");
                led2IsOn = true;
            }
            else if ((Int32.Parse(sensorsValues[1]) > 500 && Int32.Parse(sensorsValues[1]) < 600) && led2IsOn) {
                WriteToArduino("HideLed2");
                led2IsOn = false;
            }

            // Si les senseurs détectent les aimants et que les joueurs ne sont pas échangés
            if ((Int32.Parse(sensorsValues[0]) < 50 || Int32.Parse(sensorsValues[0]) > 1000) && (Int32.Parse(sensorsValues[1]) < 50 || Int32.Parse(sensorsValues[1]) > 1000) && areSwitched == false)
            {
                FindObjectOfType<AudioManager>().Play("Switch");

                // On échange tous les éléments
                tileMScript1.player = tileMScript2.player;
                tileMScript2.player = tempPlayerTM;

                obMScript1.player = obMScript2.player;
                obMScript2.player = tempPlayerOM;

                obMScript1.tileManager = obMScript2.tileManager;
                obMScript2.tileManager = tempTileManagerOM;

                camera1.backgroundColor = camera2.backgroundColor;
                camera2.backgroundColor = tempCameraColor;

                fogScript1.fogColor = fogScript2.fogColor;
                fogScript2.fogColor = tempFogColor;


                // On échange la place des joueurs
                if (p1Script.isHavingCollision || p2Script.isHavingCollision) {
                    if (p1Script.isHavingCollision && p2Script.isHavingCollision) {

                        player1.transform.position = new Vector3(posXPlayer2, player1.transform.position.y,
                            (p2Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p2Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                        player2.transform.position = new Vector3(posXPlayer1, player2.transform.position.y,
                            (p1Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p1Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                    }
                    else if(p1Script.isHavingCollision) {
                        player1.transform.position = new Vector3(posXPlayer2, player1.transform.position.y, posZPlayer2);
                        player2.transform.position = new Vector3(posXPlayer1, player2.transform.position.y,
                            (p1Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p1Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                    }
                    else {
                        player1.transform.position = new Vector3(posXPlayer2, player1.transform.position.y,
                            (p2Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p2Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                        player2.transform.position = new Vector3(posXPlayer1, player2.transform.position.y, posZPlayer1);
                    }

                }
                else {
                    player1.transform.position = new Vector3(posXPlayer2, player1.transform.position.y, posZPlayer2);
                    player2.transform.position = new Vector3(posXPlayer1, player2.transform.position.y, posZPlayer1);
                }

                // Les joueurs sont échangés
                areSwitched = true;
            }


            // Si un des deux senseurs ne détecte plus l'aimant et que les joueurs sont échangés
            else if (
               (
                 (Int32.Parse(sensorsValues[0]) > 500 && Int32.Parse(sensorsValues[0]) < 600) ||
                    (Int32.Parse(sensorsValues[1]) > 500 && Int32.Parse(sensorsValues[1]) < 600)
                )
                &&
                areSwitched == true
            )
            {

                FindObjectOfType<AudioManager>().Play("Unswitch");

                // On re-échange tous les éléments
                tileMScript1.player = tileMScript2.player;
                tileMScript2.player = tempPlayerTM;

                obMScript1.player = obMScript2.player;
                obMScript2.player = tempPlayerOM;

                obMScript1.tileManager = obMScript2.tileManager;
                obMScript2.tileManager = tempTileManagerOM;

                camera1.backgroundColor = camera2.backgroundColor;
                camera2.backgroundColor = tempCameraColor;

                fogScript1.fogColor = fogScript2.fogColor;
                fogScript2.fogColor = tempFogColor;


               // On remet les joueurs à leur place
               if (p1Script.isHavingCollision || p2Script.isHavingCollision) {
                    if (p1Script.isHavingCollision && p2Script.isHavingCollision) {

                        player1.transform.position = new Vector3(posXPlayer1, player1.transform.position.y,
                            (p2Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p2Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                        player2.transform.position = new Vector3(posXPlayer2, player2.transform.position.y,
                            (p1Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p1Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                    }
                    else if(p1Script.isHavingCollision) {
                        player1.transform.position = new Vector3(posXPlayer1, player1.transform.position.y, posZPlayer2);
                        player2.transform.position = new Vector3(posXPlayer2, player2.transform.position.y,
                            (p1Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p1Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                    }
                    else {
                        player1.transform.position = new Vector3(posXPlayer1, player1.transform.position.y,
                            (p2Script.currentObstacle.GetComponent<BoxCollider>().transform.position.z - p2Script.currentObstacle.GetComponent<BoxCollider>().bounds.size.z / 2) - 2);
                        player2.transform.position = new Vector3(posXPlayer2, player2.transform.position.y, posZPlayer1);
                    }

                }
                else {
                    player1.transform.position = new Vector3(posXPlayer1, player1.transform.position.y, posZPlayer2);
                    player2.transform.position = new Vector3(posXPlayer2, player2.transform.position.y, posZPlayer1);
                }

                areSwitched = false;
            }

        }


    }

    // Lis les données de l'arduino
    public string ReadFromArduino()
    {
        try
        {
            return stream.ReadLine();
        }
        catch (TimeoutException)
        {
            return null;
        }
    }

    public void WriteToArduino(string message) {
	    stream.WriteLine(message);
        stream.BaseStream.Flush();
    }

    // Ferme l'arduino
    public void Close()
    {
        stream.Close();
    }

    private void OnApplicationQuit()
    {
       WriteToArduino("HideLed");
       WriteToArduino("HideLed2");
    }
}
