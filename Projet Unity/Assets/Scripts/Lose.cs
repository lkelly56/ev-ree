﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script qui s'enclanchent lorsqu'on va à la scène de perte
public class Lose : MonoBehaviour
{
    public GameObject loseInterface;
    public String loseSound;

    private void Start()
    {
        // On cache l'interface "perdu", ce qui laisse le temps à l'écran noir d'apparaître
        loseInterface.SetActive(false);
        StartCoroutine(ShowLoseInterface());
    }

    private IEnumerator ShowLoseInterface()
    {
        // Après 3 secondes, on affiche l'interface "perdu"
        yield return new WaitForSeconds(3);

        if (loseSound != "blank")
        {
            FindObjectOfType<AudioManager>().Play(loseSound);
        }
        loseInterface.SetActive(true);
    }
}
