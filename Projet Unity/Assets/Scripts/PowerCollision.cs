﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerCollision : MonoBehaviour
{
    // Variable du script du joueur
    public Player playerScript;

    // Variable de l'animator du pouvoir du soleil (P1)
    public Animator fireAnim;

    // Variable du pont (P2)
    public GameObject bridge;

    // Variable déterminant quel pouvoir est exécuter
    public int whichPower;

    // Variable faisant référence à l'obstacle actuel
    private GameObject currentObstacle;

    // Quand il y a collision
    private void OnCollisionEnter(Collision colInfo)
    {
        // Si le pouvoir enclenché est celui du soleil et qu'on est en collision avec les roches
        if (whichPower == 1 && colInfo.collider.tag == "obstacleSun")
        {
            // On prend en mémoire l'obstacle et on retire les collider
            currentObstacle = colInfo.gameObject.transform.GetChild(0).gameObject;
            colInfo.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
            colInfo.gameObject.GetComponent<BoxCollider>().enabled = false;

            // Lancement des animations après 2 sec
            Invoke("PlayRocksImpactSound", 1.8f);
            Invoke("PlayRocksSound", 1.5f);
            Invoke("PlayAnimations", 2);
        }

        // Si le pouvoir enclenché est celui de la lune et qu'on est en collision avec un fossé
        else if (whichPower == 2 && colInfo.collider.tag == "missingTile")
        {
            Invoke("PlayBridgeSound", 0.2f);

            // On retire les collider
            colInfo.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
            colInfo.gameObject.GetComponent<BoxCollider>().enabled = false;

            // Construction du pont
            GameObject bridgePrefab = Instantiate(bridge) as GameObject;
            bridgePrefab.transform.position = new Vector3(colInfo.gameObject.transform.position.x-1.5f, colInfo.gameObject.transform.position.y+1.25f, colInfo.gameObject.transform.position.z+5.0f);

            // Le pont est détruit après 10 sec
            Destroy(bridgePrefab, 10f);
        }

    }

    private void PlayRocksImpactSound()
    {
        FindObjectOfType<AudioManager>().Play("RocksImpact");
    }

    private void PlayRocksSound()
    {
        FindObjectOfType<AudioManager>().Play("Rocks");
    }

    private void PlayBridgeSound()
    {
        FindObjectOfType<AudioManager>().Play("Bridge");
    }

    // Jouer les animations pour le pouvoir du soleil
    private void PlayAnimations()
    {
        currentObstacle.GetComponent<Animator>().SetTrigger("explode");
        fireAnim.SetBool("canExplose", true);
        currentObstacle = null;
    }
}
