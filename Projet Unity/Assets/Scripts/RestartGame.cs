﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    public float delay;

    private void Start()
    {
        // Déclenche la coroutine
        StartCoroutine(Restart());
    }

    private IEnumerator Restart()
    {
        // Repart le jeu après le délai indiqué
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(0);
    }
}
