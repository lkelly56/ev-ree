﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour
{
    // Tableau contenant les prefabs des tuiles
    public GameObject[] tilePrefabs;

    // Référence au joueur (affecté dans unity)
    public GameObject player;

    // Position en Z d'appartition
    public float spawnZ = -15.0f;

    // Longueur des tuiles
    public float tileLength = 15.0f;

    // Nombre de tuiles en tout temps dans le jeu
    public int amountTilesOnScreen = 10;

    // Liste des tuiles présentes dans le jeu
    public List<GameObject> activeTiles;

    // Safe zone du joueur
    public float safeZone = 20.0f;

    // Variable déterminant si la dernière tuile apparu est un trou ou non
    private bool lastTileIsHole = false;

    // Variable du timer
    public Timer time;


    private void Start()
    {
        // Affectation de la liste
        activeTiles = new List<GameObject>();

        // On affiche les tuiles au début du jeu
        for(int i = 0; i < amountTilesOnScreen; i++)
        {
            SpawnTile();
        }
    }

    private void Update()
    {
        // Si le joueur est à une position respectable pour afficher la prochaine tuile
       if (player.transform.position.z - safeZone > (spawnZ - amountTilesOnScreen * tileLength))
       {
            // On détruit la première tuile et on en ajoute une
            SpawnTile();
            DeleteTile();
       }
    }

    // Ajoute une tuile
    private void SpawnTile()
    {
        GameObject go;
        GameObject whichTile = tilePrefabs[0];

        // Détermine un num random entre 0 et 100 (probabilité d'affichage d'un fossé)
        float num = Random.Range(0, 100.0f);

        // Si c'est entre 0 et 10, on affiche un fossé
        if (num >= 0 && num <= 10 && activeTiles.Count >= 8 && !lastTileIsHole && time.timeLeft > 30)
        {
            whichTile = tilePrefabs[tilePrefabs.Length - 1];
            lastTileIsHole = true;
        }

        // Sinon on affiche une tuile normale
        else
        {
            int randTile = Random.Range(0, tilePrefabs.Length - 2);

            whichTile = tilePrefabs[randTile];

            lastTileIsHole = false;
        }

        // Instantiation et ajout de la tuile
        go = Instantiate(whichTile) as GameObject;
        go.transform.position = new Vector3(player.transform.position.x, go.transform.position.y, spawnZ);

        spawnZ += tileLength;

        activeTiles.Add(go);
    }

    // Détruit une tuile
    private void DeleteTile(int index = 0)
    {
        Destroy(activeTiles[index]);
        activeTiles.RemoveAt(index);
    }
}
