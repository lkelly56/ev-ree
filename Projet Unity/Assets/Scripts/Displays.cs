﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Displays : MonoBehaviour
{
    // Active les deux écrans au lancement du jeu
    private void Start()
    {
        for(int i = 0; i < Display.displays.Length; i++) {
            Display.displays[i].Activate();
        }
    }
}
