using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading;
using WiimoteApi;
using WiimoteApi.Internal;

public class Player : MonoBehaviour
{
    // Variable déterminant à quel joueur on fait référence
    public int whichPlayer;

    // Variable Rigidbody faisant référence au component du joueur (affecté dans Unity)
    public Rigidbody rb;

    // Variable BoxCollider faisant référence au component du joueur (affecté dans Unity)
    public CapsuleCollider col;

    public Animator playerAnim;

    private float posYInit;

    // Vitesse du joueur
    public float speedZ = 450.0f;

    // Booléen qui détermine si on peut bouger ou non
    public bool canMove = true;

    // Booléen qui détermine si on est en collision ou non
    public bool isHavingCollision = false;

    public GameObject currentObstacle;

    // Force du saut
    public float jumpForce = 7f;

    // Variable pour les inputs
    bool zKeyReleased = false;
    bool xKeyReleased = false;

    // Degré de "fall"
    private float fallMultiplier = 2.5f;

    // Variable pour déterminer si on touche au sol ou non
    public LayerMask groundLayers;

    // Évènement sur les touches du clavier
    private UnityEvent keyUpEvent = new UnityEvent();

    /*---------------------------------------------------------- */

    // Variable faisant référence au script des Wii remotes
    public WiiRemotes scriptRemotes;

    // Variable la wii remote du joueur
    private Wiimote playerRemote;

    // Variable du délai entre les lancés
    private float timeBetweenShots = 1f;

    // Variable du timestamp
    float timestamp;

    // Variable contenant le projectile
    public GameObject power;

    // Variable déterminant si le joueur est en train de jeter un sort ou non
    public bool isCastingSpell = false;

    /*---------------------------------------------------------- */

    private void Start()
    {
        // On empêche le joueur via le rigidbody de faire des rotation
        rb.freezeRotation = true;

        // On ajoute un écouteur sur l'évènement
        keyUpEvent.AddListener(JumpEvent);

        posYInit = 0.5575377f;

        // Initialisation des Wii remotes
        if (scriptRemotes.wiiRemotes.Count <= 0)
        {
            scriptRemotes.InitWiimotes();
        }

        // Assignation des Wii Remotes aux joueurs
        if (scriptRemotes.wiiRemotes.Count >= 2)
        {
                if (whichPlayer == 1)
                {
                    playerRemote = scriptRemotes.wiiRemotes[0];
                    Debug.Log("Player 1 : " + playerRemote);
                }
                else if (whichPlayer == 2)
                {
                    playerRemote = scriptRemotes.wiiRemotes[1];
                    Debug.Log("Player 2 : " + playerRemote);
                }
        }
    }

    private void FixedUpdate()
    {
        // Si le personnage est en collision et qu'il a un obstacle devant lui
        if (isHavingCollision && currentObstacle != null) {
            float posYPlayer = transform.position.y - posYInit;
            float posYObstacle = currentObstacle.GetComponent<BoxCollider>().bounds.size.y;

            // S'il est plus haut que l'obstacle, il peut continuer d'avancer
            if (posYPlayer >= posYObstacle) {

                canMove = true;
                isHavingCollision = false;
                playerAnim.SetBool("isIdle", false);
            }
        }


        // Si le personnage peut bouger et qu'il n'est pas en collision, il peut avancer
        if (canMove && isHavingCollision == false)
        {
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, speedZ * Time.deltaTime);
        }
    }

    private void Update()
    {

        if (!IsGrounded())
        {
            // Simule une augmentation de gravité pour rendre le saut plus rapide
            rb.AddForce(Vector3.down * (jumpForce * 2f));
        }

        // Saut joueur 1
        if (whichPlayer == 1)
        {
            zKeyReleased = Input.GetKeyUp("z");

            if (zKeyReleased && keyUpEvent != null && IsGrounded() && !isCastingSpell)
            {
               keyUpEvent.Invoke();
               FindObjectOfType<AudioManager>().Play("JumpNar");
            }

            StartCoroutine(JumpDelay());

        }

        // Saut joueur 2
        else if (whichPlayer == 2)
        {

            xKeyReleased = Input.GetKeyUp("x");

            if (xKeyReleased && keyUpEvent != null && IsGrounded() && !isCastingSpell)
            {
               keyUpEvent.Invoke();
               FindObjectOfType<AudioManager>().Play("JumpSar");
            }

            StartCoroutine(JumpDelay());
        }

        // On lit les données de la Wii Remote;
        if (playerRemote != null)
        {
            readPlayerRemoteData();
        }
    }

    // Délai entre chaque saut
    IEnumerator JumpDelay()
    {
        yield return new WaitForSeconds(2);
    }

    // Jump
    private void JumpEvent() {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        playerAnim.SetTrigger("jump");
    }

    // Vérifie si on est proche du sol ou non
    private bool IsGrounded() {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    }


    // Détecte quand on entre en collision avec un obstacle
    private void OnCollisionEnter(Collision leCol)
    {
        if (leCol.gameObject.tag == "obstacleSun" || leCol.gameObject.tag == "obstacleNormal")
        {
            canMove = false;
            isHavingCollision = true;
            currentObstacle = leCol.gameObject;

            playerAnim.SetBool("isIdle", true);
        }

        if (leCol.gameObject.tag == "missingTile")
        {
            canMove = false;
            isHavingCollision = true;
            currentObstacle = leCol.gameObject;

            playerAnim.SetBool("isIdle", true);
            rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

        if (leCol.gameObject.tag == "bridge")
        {
            isHavingCollision = false;
            currentObstacle = null;
        }
    }

    // Détecte quand on quitte une collision avec un obstacle
    private void OnCollisionExit(Collision leCol)
    {
        if (leCol.gameObject.tag == "obstacleSun" || leCol.gameObject.tag == "obstacleNormal" || leCol.gameObject.tag == "missingTile")
        {
            canMove = true;
            isHavingCollision = false;
            currentObstacle = null;

            playerAnim.SetBool("isIdle", false);

            if (leCol.gameObject.tag == "missingTile")
            {
                rb.constraints &= ~RigidbodyConstraints.FreezePositionZ;
            }
        }
    }

    private void readPlayerRemoteData()
    {   
        // Variable pour contenir le data de la remote
        int data;

        // Tant qu'il y a des données à lire
        do
        {
            // Lire le data
            data = playerRemote.ReadWiimoteData();

            // Aller chercher les données de l'accélération
            float[] accel = playerRemote.Accel.GetCalibratedAccelData();

            // Prendre le résultat de l'accélération
            float accel_result = accel[0];


            // Si l'accélération est plus haut que 2 et qu'au moins une seconde s'est écoulée
            if (accel_result > 2 && playerRemote.Button.b == true && Time.time >= timestamp)
            {

                // On lance le pouvoir
                ThrowPower();

                // Update du temps
                timestamp = Time.time + timeBetweenShots;
            }

        } while (data > 0);
    }

    // Lance le pouvoir du joueur
    private void ThrowPower () {
        playerAnim.SetTrigger("spellCast");
        canMove = false;
        isCastingSpell = true;
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        // On instantie le pouvoir
        GameObject powerAnimation = Instantiate(power) as GameObject;

        if (whichPlayer == 1)
        {
            FindObjectOfType<AudioManager>().Play("PowerNar");
        }
        else if (whichPlayer == 2)
        {
            FindObjectOfType<AudioManager>().Play("PowerSar");
        }

        // On le place à la position du joueur
        powerAnimation.transform.position = new Vector3(transform.position.x, transform.position.y+0.75f, transform.position.z+1.75f);

        Destroy(powerAnimation, 2.5f);
    }
}
