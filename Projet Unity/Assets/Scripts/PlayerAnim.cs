﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    public Player player;
    public Animator playerAnimator;

    // Fonction appelée à la fin de l'animation du spell casting
    public void stopSpellCasting()
    {
        player.canMove = true;
        player.isCastingSpell = false;

        if (player.currentObstacle == null)
        {
            playerAnimator.SetBool("isIdle", false);
        }

        player.rb.constraints &= ~RigidbodyConstraints.FreezePositionZ;
    }
}
