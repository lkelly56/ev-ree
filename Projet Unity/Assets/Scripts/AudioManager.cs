﻿using System;
using UnityEngine.Audio;
using UnityEngine;


// Script tiré d'un tutoriel de Brackeys; https://www.youtube.com/watch?v=6OT43pvUyfY
public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public String theme;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {

 
        Play(theme);
    }

    public void Play (string sound)
    {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Le son " + name + " n'a pas été trouvé !");
            return;
        }

        s.source.Play();
        //FindObjectOfType<AudioManager>().Play("nameofsound");
    }
}
