﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
   // Est appelé lorsque l'on clique sur le bouton "Commencer le jeu"
   public void StartGame()
   {
        FindObjectOfType<AudioManager>().Play("Button");

        Invoke("PlayGame", 1);
   }

    private void PlayGame()
    {
        // Part le jeu
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
