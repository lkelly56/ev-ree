﻿using UnityEngine;

// Script relié à Player 1 Camera
public class FollowPlayer : MonoBehaviour
{
    // Référence au joueur
    public GameObject player;

    // Variable offset dont les valeurs sont établies dans Unity
    public Vector3 offset;

    // Variable du timer
    public Timer time;

    // Détermine si l'animation de début des caméras est finie ou non
    private bool animationEnded = false;

    private void Update()
    {
        // Si le timer est plus haut que 1.5 secondes
        if (time.timeLeft > 1.5f)
        {
            // Si l'animation de départ n'est pas finie
            if (transform.position.y > 2.76 && animationEnded == false)
            {
                // On joue l'animation
                transform.position -= new Vector3(0, 1.7f * Time.deltaTime, 0);
                transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + offset.z);
            }
            // Sinon
            else
            {
                animationEnded = true;

                // Fait en sorte que la caméra suit le joueur avec un décalage (offset)
                transform.position = player.transform.position + offset;
            }
        }
    }
}
