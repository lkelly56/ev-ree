﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Monster : MonoBehaviour
{
    // Variable faisant référence au système de particules
    public ParticleSystem smoke;

    // Variable déterminant si le système de particules joue ou non
    private bool isPlaying = false;

    // Variable faisant référence au joueur
    public Player player;

    // Variable du canvas du texte
    public GameObject monsterText;

    // Différence de distance entre le milieu du système de particule et le bout
    private float diffDistance = 0;

    // Variable déterminant si le 2e son du monstre a joué ou non
    private bool soundHasPlayed = false;

    private void Start()
    {
        // On cache l'interface au début
        monsterText.SetActive(false);

        // On va chercher la distance et on désactive le collider
        diffDistance = GetComponent<BoxCollider>().bounds.size.z / 2;
        GetComponent<BoxCollider>().enabled = false;

        // Dès le début, on empêche le système de particules de s'exécuter
        smoke.Stop();
    }

    private void Update()
    {
        // On fait avancer le "monstre"
        transform.position += new Vector3(0, 0, 5 * Time.deltaTime);

        // Position au bout du fog
        float fogPosZ = transform.position.z + diffDistance;
        
        // Si on est proche du joueur et que le système n'est pas parti
        if (fogPosZ >= (player.transform.position.z - 5) && !isPlaying)
        {
            // On affiche le texte
            monsterText.SetActive(true);

            Invoke("RemoveText", 3.5f);

            FindObjectOfType<AudioManager>().Play("MonsterIsClose");

            // On le part
            smoke.Play();
            isPlaying = true;
        }

        // Si le joueur s'est éloigné et que le système est parti
        else if (fogPosZ < (player.transform.position.z - 5) && isPlaying)
        {
            // On l'arrête
            smoke.Stop();
            isPlaying = false;
        }

        if (transform.position.z >= player.transform.position.z - 30 && !soundHasPlayed)
        {
            FindObjectOfType<AudioManager>().Play("MonsterIsReallyClose");
            soundHasPlayed = true;
        }
        else if (transform.position.z < player.transform.position.z - 30 && soundHasPlayed)
        {
            soundHasPlayed = false;
        }

        // Si le monstre a attrapé le joueur
        if (transform.position.z >= (player.transform.position.z - player.GetComponent<CapsuleCollider>().radius) && 
            transform.position.z <= (player.transform.position.z + player.GetComponent<CapsuleCollider>().radius))
        {
            // On lance la scène de perte
            SceneManager.LoadScene(3);
        }
    }

    private void RemoveText()
    {
        monsterText.SetActive(false);
    }
}
